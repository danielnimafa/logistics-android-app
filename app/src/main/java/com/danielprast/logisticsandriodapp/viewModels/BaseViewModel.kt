package com.danielprast.logisticsandriodapp.viewModels

abstract class BaseViewModel {

    val bindListeners: MutableList<Box<Any>> = mutableListOf()

    open fun unbindBoxes() {
        if (bindListeners.isNotEmpty())
            bindListeners.forEach { it.unbind() }
    }

    open fun onCleared() {
        unbindBoxes()
        bindListeners.clear()
    }

}