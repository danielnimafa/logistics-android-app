package com.danielprast.logisticsandriodapp.viewModels

import com.danielprast.logisticsandriodapp.models.ExampleDataModel
import kotlin.properties.Delegates


class ExampleItemScanViewModel() {

    private var container: ExampleDataModel.Item by Delegates.observable(
        ExampleDataModel.Item()
    ) { _, _, newValue ->
        newValue.let {
            code.value = it.code
        }
    }

    var id = Box("")
    var code = Box("")
    var warningMessage = Box("")

    fun updateContainer(container: ExampleDataModel.Item) {
        this.container = container
    }

    fun getDataContainer(): ExampleDataModel.Item {
        return this.container
    }
}