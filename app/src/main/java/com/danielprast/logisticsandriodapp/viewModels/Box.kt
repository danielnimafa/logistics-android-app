package com.danielprast.logisticsandriodapp.viewModels

import kotlin.properties.Delegates

class Box<T>(value: T) {

    var listener: ((T) -> Unit)? = null

    var value: T by Delegates.observable(value) { _,_, newValue ->
        listener?.invoke(newValue)
    }

    init {
        this.value = value
    }

    fun bind(listener: ((T) -> Unit)?) {
        this.listener = listener
        //listener?.invoke(this.value)
    }

    fun unbind() {
        listener = null
    }

}