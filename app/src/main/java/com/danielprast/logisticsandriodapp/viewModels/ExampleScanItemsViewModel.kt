package com.danielprast.logisticsandriodapp.viewModels

import android.app.Activity
import com.danielprast.logisticsandriodapp.R
import com.danielprast.logisticsandriodapp.models.ExampleDataModel
import com.danielprast.logisticsandriodapp.utils.DataService.ScannedItemsService
import com.danielprast.logisticsandriodapp.utils.extension.stringGet
import com.danielprast.logisticsandriodapp.utils.shout
import com.danielprast.logisticsandriodapp.utils.traceException
import com.google.gson.Gson
import kotlin.properties.Delegates

class ExampleScanItemsViewModel : BaseViewModel() {

    var activity: Activity? = null

    private var dataModel: ExampleDataModel.ScanItemsData by Delegates.observable(
        ExampleDataModel.ScanItemsData()
    ) { _, _, newValue ->
        shout("Updated Items Size", newValue.items.size)
        shout("Updated Items", newValue.items)
        _listOfDidScanItems.value = newValue.items
    }

    private val _listOfDidScanItems: Box<List<ExampleDataModel.Item>> = Box(listOf())
    val listOfDidScanItems: Box<List<ExampleDataModel.Item>> = _listOfDidScanItems

    val formattedItemCounter: String
        get() {
            val label = activity?.stringGet(R.string.str_show_all) ?: "Show All"
            return "$label (${dataModel.items.size})"
        }

    val lastScanCodeLabel: String
        get() {
            val label = activity?.stringGet(R.string.str_latest_code_item) ?: "Latest item"
            val latestItem = try {
                dataModel.items[0].code
            } catch (e: Exception) {
                ""
                traceException(e)
            }
            return "$label:\n${latestItem}"
        }

    init {
        bindListeners.add(listOfDidScanItems as Box<Any>)
    }

    private val dataModelService = ScannedItemsService()

    fun initItems() {
        val savedScanningProgress = dataModelService.loadItems()
        updateListOfDidScanItems(savedScanningProgress)
    }

    fun saveModelData() {
        val json = Gson().toJson(dataModel)
        dataModelService.saveScanningProgress(json)
    }

    fun appendNewItem(code: String) {
        val arrItems: MutableList<ExampleDataModel.Item> = mutableListOf()
        arrItems.addAll(dataModel.items)
        arrItems.add(
            0,
            ExampleDataModel.Item(
                code
            )
        )
        updateListOfDidScanItems(arrItems)
    }

    fun updateListOfDidScanItems(list: List<ExampleDataModel.Item>) {
        val dataModelToUpdate = dataModel
        dataModelToUpdate.items.clear()
        dataModelToUpdate.items.addAll(list)
        dataModel = dataModelToUpdate
    }

}