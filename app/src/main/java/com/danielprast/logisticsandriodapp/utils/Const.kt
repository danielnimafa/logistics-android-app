package com.danielprast.logisticsandriodapp.utils

import android.Manifest
import android.content.pm.PackageManager

/**
 * Created by danielnimafa on 02/27/18.
 */

object Const {

    object DevMode {
        const val ENABLE = 1
        const val DISABLE = 0
    }

    const val app_name = "APP_NAME" // replace with your own app name

    const val ROOT_URL = "base_url"
    const val endpoint_test = "api.staging.parseltruk.com"
    const val endpoint = "api.parseltruk.com"
    const val BASE_URL_TEST = "https://$endpoint_test/v1/"
    const val BASE_URL = "https://$endpoint/v1/"

    const val CAMERA_REQUEST_CODE = 10

    const val multipartFormdata = "multipart/form-data"

}