package com.danielprast.logisticsandriodapp.utils

import java.util.*

class DateHelper {
    companion object {
        val instance = DateHelper()
    }

    fun formatCurrentDate(c: Calendar): String {

        val year = c.get(Calendar.YEAR)
        var month = c.get(Calendar.MONTH)
        val day = c.get(Calendar.DAY_OF_MONTH)
        val dash = "-"
        var strMonth = ""
        month += 1
        if (month < 10) {
            strMonth = "0" + month.toString()
        } else {
            strMonth = month.toString()
        }

        return year.toString() + dash + strMonth + dash +
                day.toString()
    }

    fun handleDate(position: Int) {

        val c = Calendar.getInstance()
        c.time = Date()

        val strCurrentDate = formatCurrentDate(c)

        shout("strCurrentDate", strCurrentDate)

        when (position) {
            0 -> c.add(Calendar.DAY_OF_YEAR, -3)
            1 -> c.add(Calendar.DAY_OF_YEAR, -7)
            2 -> c.add(Calendar.DAY_OF_YEAR, -30)
            3 -> c.add(Calendar.DAY_OF_YEAR, 0)
        }

        val strStartDate = formatCurrentDate(c)
        shout("strStartDate", strStartDate)
    }
}