package com.danielprast.logisticsandriodapp.utils.extension

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.Context.INPUT_METHOD_SERVICE
import android.content.Intent
import android.content.pm.PackageInfo
import android.content.pm.PackageManager
import android.media.MediaPlayer
import android.os.Build
import android.os.Handler
import android.os.Vibrator
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.danielprast.logisticsandriodapp.utils.Const
import com.danielprast.logisticsandriodapp.utils.shout
import com.danielprast.logisticsandriodapp.utils.traceException


/*
 * Created by danielnimafa on 02/27/18.
 */

fun Activity.vibrateDevice() {
    val vib = getSystemService(Context.VIBRATOR_SERVICE) as Vibrator
    vib.vibrate(100)
}

fun Activity.playScanSound() {
    try {
        val afd = assets.openFd("audio/plucky.mp3")
        MediaPlayer().apply {
            setDataSource(afd.fileDescriptor, afd.startOffset, afd.length)
            prepare()
            start()
        }
    } catch (e: Exception) {
        traceException(e)
    }
}

fun Activity.getarinAja() {
    val vib = getSystemService(Context.VIBRATOR_SERVICE) as Vibrator
    vib.vibrate(100)
}

inline fun <reified T: AppCompatActivity> getScreen(context: Context) = Intent(context, T::class.java)

fun Activity.playSound() {
    try {
        val afd = assets.openFd("audio/plucky.mp3")
        MediaPlayer().apply {
            setDataSource(afd.fileDescriptor, afd.startOffset, afd.length)
            prepare()
            start()
        }
    } catch (e: Exception) {
        traceException(e)
    }
}

fun Activity.hideSoftKeyboard(ed: EditText) {
    val imm = this.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    imm.hideSoftInputFromWindow(ed.getWindowToken(), 0)
}

fun Activity.hideSoftKeyboard() {
    currentFocus?.also {
        val inputMethodManager = getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager
        inputMethodManager.hideSoftInputFromWindow(it.windowToken, 0)
    }
}

fun Activity.hideCurrentSoftKeyboard() {
    window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
}

fun Activity.verifyPermission(granResult: IntArray): Boolean {
    if (granResult.isEmpty()) {
        return false
    }

    granResult.filter { it != PackageManager.PERMISSION_GRANTED }.forEach {
        return false
    }

    return true
}

fun Context.getPackageAppInfo(): PackageInfo? {
    var info: PackageInfo? = null
    try {
        info = this.packageManager.getPackageInfo(packageName, 0)
    } catch (err: PackageManager.NameNotFoundException) {
        traceException(err)
    }
    return info
}

@Throws(PackageManager.NameNotFoundException::class)
fun Context.getAppVersionCode(): Long {
    val pinfo = packageManager.getPackageInfo(packageName, 0)
    var returnValue = pinfo.versionCode.toLong()

    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
        returnValue = pinfo.longVersionCode
    }

    shout("AAA", "Full long value: $returnValue")
    shout("AAA", "Major Version Code (your chosen one) " + (returnValue shr 32)) // 8 in this scenario
    shout("AAA", "Version Code (your chosen one) " + (returnValue and -0x1).toInt()) // 127 in this scenario

    return returnValue
}

fun Fragment.requestCameraPermission() {
    val listOfPerm = arrayOf(Manifest.permission.CAMERA,
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE)
    requestPermissions(listOfPerm, Const.CAMERA_REQUEST_CODE)
}

fun isThisDeviceSupportCamera(appContext: Context): Boolean =
        appContext.packageManager.hasSystemFeature(PackageManager.FEATURE_CAMERA)

fun postDelayed(delayMillis: Long, task: () -> Unit) {
    Handler().postDelayed(task, delayMillis)
}