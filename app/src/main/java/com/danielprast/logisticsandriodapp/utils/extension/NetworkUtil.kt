package com.danielprast.logisticsandriodapp.utils.extension

import android.app.Activity
import android.content.Context
import android.net.ConnectivityManager
import com.danielprast.logisticsandriodapp.R
import com.danielprast.logisticsandriodapp.utils.Const
import okhttp3.Headers
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import org.jetbrains.anko.toast
import java.io.File
import java.net.SocketException
import java.net.SocketTimeoutException
import java.net.UnknownHostException

/*
 * Created by danielnimafa on 02/27/18.
 */

fun Activity.isOnline(offline: () -> Unit = { toast(stringGet(R.string.message_internet_lost)) }, online: () -> Unit = {}) {
    if (checkOnlineStatus()) online() else offline()
}

fun Activity.checkOnlineStatus(): Boolean {
    val connManager = this.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
    val networkInfo = connManager.activeNetworkInfo
    return (networkInfo != null && networkInfo.isConnected)
}

fun Activity.getNetworkingError(e: Throwable): String {

    val msgError: String? = if (e is SocketTimeoutException) {
        stringGet(R.string.timeout_msg)
    } else if (e is SocketException || e is UnknownHostException) {
        stringGet(R.string.server_lost_conn)
    } else if (e is NullPointerException) {
        stringGet(R.string.invalid_response)
    } else {
        e.message
    }

    return msgError ?: ""
}

fun populateErrorMessages(errors: List<String>?): String {
    val sb = StringBuilder()
    errors?.run {
        val count = this.size
        if (count > 0) this.forEachIndexed { index, s ->
            sb.append("• $s" + if (index == count - 1) "" else "\n")
        }
    }
    return sb.toString()
}

fun createRequestBody(value: String?, key: String? = Const.multipartFormdata): RequestBody {
    return RequestBody.create(MediaType.parse(key!!), value!!)
}

fun createMultipartFile(key: String, file: File): MultipartBody.Part {
    val rbAttachment = RequestBody.create(MediaType.parse(Const.multipartFormdata), file)
    return MultipartBody.Part.create(Headers.of("Content-Disposition",
            "form-data; name=\"$key\"; filename=\"${file.name}\""), rbAttachment)
}