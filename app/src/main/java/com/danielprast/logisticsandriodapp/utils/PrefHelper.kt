package com.danielprast.logisticsandriodapp.utils

import com.pixplicity.easyprefs.library.Prefs

object PrefHelper {

    //region Base URL
    var baseURL: String
        get() {
            val baseUrl = Prefs.getString(Const.ROOT_URL, "")
            if (baseUrl.isEmpty()) return Const.BASE_URL
            return baseUrl
        }
        set(value) {
            Prefs.putString(Const.ROOT_URL, value)
            shout("saved new URL", baseURL)
        }


    fun loadBaseURL(): String {
        val baseUrl = Prefs.getString(Const.ROOT_URL, "")
        if (baseUrl.isEmpty()) return Const.BASE_URL_TEST
        return baseUrl
    }

    fun saveBaseURL(url: String) {
        Prefs.putString(Const.ROOT_URL, url)
        shout("saved new URL", baseURL)
    }
    //endregion

    val isDevelopment: Boolean
        get() = developmentMode == Const.DevMode.ENABLE

    var developmentMode: Int
        get() = Prefs.getInt("mode", Const.DevMode.DISABLE)
        set(value) = Prefs.putInt("mode", value)

    fun loadAppName(): String = Prefs.getString("appname", "")
    fun saveAppName(name: String) {
        Prefs.putString("appname", name)
        shout("App Name", loadAppName())
    }

}