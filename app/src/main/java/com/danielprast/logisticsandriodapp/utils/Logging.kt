package com.danielprast.logisticsandriodapp.utils

fun shout(key: String, any: Any?) {
    if (PrefHelper.isDevelopment) println("😎 $key: $any")
}

fun traceException(e: Exception) {
    if (PrefHelper.isDevelopment) e.printStackTrace()
}

fun thisContext(activityClass: Class<*>) {
    shout("View Context", activityClass.simpleName)
}