package com.danielprast.logisticsandriodapp.utils.DataService

import com.danielprast.logisticsandriodapp.models.ExampleDataModel
import com.google.gson.Gson
import com.pixplicity.easyprefs.library.Prefs


class ScannedItemsService {

    private val tag_items = "scanItemsResult"
    var scannedItemsJson: String
        get() = Prefs.getString(tag_items, "")
        set(value) = Prefs.putString(tag_items, value)

    fun loadItems(): List<ExampleDataModel.Item> {
        if (scannedItemsJson.isEmpty()) {
            scannedItemsJson = Gson().toJson(ExampleDataModel.ScanItemsData())
        }
        val obj = Gson().fromJson(scannedItemsJson, ExampleDataModel.ScanItemsData::class.java)
        return obj.items
    }

    fun saveScanningProgress(json: String) {
        scannedItemsJson = json
    }

}