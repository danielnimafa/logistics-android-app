package com.danielprast.logisticsandriodapp.modules

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.danielprast.logisticsandriodapp.R

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}
