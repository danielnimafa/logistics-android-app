package com.danielprast.logisticsandriodapp.modules.ScanCamera

import android.content.pm.PackageManager
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.app.ActivityCompat
import androidx.fragment.app.Fragment
import com.danielprast.logisticsandriodapp.R
import com.danielprast.logisticsandriodapp.utils.Const
import com.danielprast.logisticsandriodapp.utils.extension.*
import com.danielprast.logisticsandriodapp.utils.shout
import com.danielprast.logisticsandriodapp.views.PopupProgress
import com.google.zxing.Result
import kotlinx.android.synthetic.main.fragment_scanner_layout.*
import me.dm7.barcodescanner.zxing.ZXingScannerView
import kotlin.properties.Delegates


abstract class BaseScannerFragment : Fragment(),
    ZXingScannerView.ResultHandler {

    private var scannerView: ZXingScannerView? = null
    private var isActiveScan = false
    private var isFlashDidActive = false
    private var activeImage: Drawable? = null
    private var inactiveImage: Drawable? = null
    private var flashOnImage: Drawable? = null
    private var flashOffImage: Drawable? = null
    var progressView: PopupProgress? = null
    var titleTextString: String by Delegates.observable("") { _, _, _ ->
        setupTitleScreen()
    }
    var subtitleTextString: String by Delegates.observable("") { _, _, _ ->
        setupSubtitleScreen()
    }

    private val scanFragmentTag: String
        get() {
            return setupScanFragmentTag()
        }

    private val fragmentListResultTag: String
        get() {
            return setupFragmentListResultTag()
        }

    private val fragmentListResult: Fragment
        get() {
            return setupFragmentListResult()
        }

    /**
     * Method untuk assigning properti Tag-nya fragment scanner
     */
    abstract fun setupScanFragmentTag(): String

    /**
     * Method untuk assigning properti fragment list
     */
    abstract fun setupFragmentListResult(): Fragment

    /**
     * Method untuk assigning properti Tag-nya fragment list
     */
    abstract fun setupFragmentListResultTag(): String

    /**
     * Dipanggil ketika kode telah didapatkan
     *
     * @param code Parameter tidak nullable, jadi kalo kosong ya valuenya string kosongan
     */
    abstract fun didSetCode(code: String)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activity?.run {
            progressView = PopupProgress(this)
        } ?: run { throw Exception("Invalid Activity") }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(
            R.layout.fragment_scanner_layout,
            container,
            false
        )
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupView()
    }

    override fun onResume() {
        super.onResume()
        activity?.run {
            scannerView?.run {
                setResultHandler(this@BaseScannerFragment)
                if (isActiveScan) startCamera();
            }
        }
    }

    override fun onPause() {
        super.onPause()
        scannerView?.stopCamera()
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        if (requestCode == Const.CAMERA_REQUEST_CODE) checkingCameraPermission()
        super.onRequestPermissionsResult(
            requestCode,
            permissions,
            grantResults
        )
    }

    override fun handleResult(scanResult: Result?) {
        activity?.run {
            playScanSound()
            vibrateDevice()
            postDelayed(1000) {
                scannerView?.resumeCameraPreview(this@BaseScannerFragment)
            }
        }

        val code = scanResult?.text?.toString() ?: "-"
        shout("Scan", code)
        edInputCode.setText(code)
        didSetCode(code)
    }

    private fun didTapScanButton() {
        activity?.run {
            val inputCode = edInputCode.text.toString()
            if (inputCode.isNotEmpty()) {
                hideCurrentSoftKeyboard()
                didSetCode(inputCode)
            }
        }
    }

    private fun didTapShowItemsButton() {
        activity?.run {
            supportFragmentManager.beginTransaction()
                .setCustomAnimations(
                    R.anim.enter_from_bottom,
                    R.anim.exit_to_top,
                    R.anim.enter_from_top,
                    R.anim.exit_to_bottom
                )
                .addToBackStack(scanFragmentTag)
                .replace(
                    R.id.fragment_container_layout,
                    fragmentListResult,
                    fragmentListResultTag
                )
                .commit()
        }
    }

    private fun didTapVisibleButton() {
        isActiveScan = if (isActiveScan) {
            visible_btn.setImageDrawable(inactiveImage)
            scannerView?.stopCamera()
            false
        } else {
            visible_btn.setImageDrawable(activeImage)
            scannerView?.startCamera()
            true
        }
    }

    private fun didTapflashButton() {
        isFlashDidActive = if (isFlashDidActive) {
            flash_btn.setImageDrawable(flashOffImage)
            false
        } else {
            flash_btn.setImageDrawable(flashOnImage)
            true
        }
        scannerView?.flash = isFlashDidActive
    }

    private fun setupView() {
        activity?.run {
            activeImage = drawableGet(R.drawable.active_vis)
            inactiveImage = drawableGet(R.drawable.inactive_vis)
            flashOnImage = drawableGet(R.drawable.ic_flash_on)
            flashOffImage = drawableGet(R.drawable.ic_flash_off)

            scannerView = ZXingScannerView(this)
            scanner_content.addView(scannerView)

            isActiveScan = true
            visible_btn.setImageDrawable(activeImage)
            visible_btn.click { didTapVisibleButton() }

            flash_btn.setImageDrawable(flashOffImage)
            flash_btn.click { didTapflashButton() }

            cameraPermBtn.click { requestCameraPermission() }
            checkingCameraPermission()

            topSpace.layoutParams.height = getStatusBarHeight()
            bottomSpace.layoutParams.height = if (hasNavBar(resources)) getNavBarHeight() else 0

            subtitleTextString = ""

            scanBtn.click {
                didTapScanButton()
            }

            button_show_items.click {
                didTapShowItemsButton()
            }
        }
    }

    private fun setupTitleScreen() {
        txTitleName.text = titleTextString
    }

    private fun setupSubtitleScreen() {
        if (subtitleTextString.isEmpty()) {
            txSubtitleName.gone()
            return
        }
        txSubtitleName.text = subtitleTextString
        txSubtitleName.visible()
    }

    private fun checkingCameraPermission() {
        activity?.run {
            val granted = PackageManager.PERMISSION_GRANTED
            val cameraPerm = ActivityCompat.checkSelfPermission(
                this,
                android.Manifest.permission.CAMERA
            )
            val readStoragePerm = ActivityCompat.checkSelfPermission(
                this, android.Manifest.permission.READ_EXTERNAL_STORAGE
            )
            val writeStoragePerm = ActivityCompat.checkSelfPermission(
                this,
                android.Manifest.permission.WRITE_EXTERNAL_STORAGE
            )

            val isPermissionGranted = (
                    cameraPerm == granted &&
                            readStoragePerm == granted &&
                            writeStoragePerm == granted &&
                            writeStoragePerm == granted
                    )

            isPermissionGranted.run {
                showCamera(this)
                showCameraPermissionLayout(!this)
            }
        }
    }

    private fun showCamera(state: Boolean) {
        scanner_content.visibility = if (state) View.VISIBLE else View.GONE
    }

    private fun showCameraPermissionLayout(state: Boolean) {
        camera_permission_layout.visibility = if (state) View.VISIBLE else View.GONE
    }

}