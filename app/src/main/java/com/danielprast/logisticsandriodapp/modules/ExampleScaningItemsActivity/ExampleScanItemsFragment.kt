package com.danielprast.logisticsandriodapp.modules.ExampleScaningItemsActivity

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import com.danielprast.logisticsandriodapp.modules.ScanCamera.BaseScannerFragment
import com.danielprast.logisticsandriodapp.utils.extension.gone
import com.danielprast.logisticsandriodapp.utils.extension.visible
import com.danielprast.logisticsandriodapp.utils.shout
import com.danielprast.logisticsandriodapp.utils.thisContext
import com.danielprast.logisticsandriodapp.viewModels.ExampleScanItemsViewModel
import kotlinx.android.synthetic.main.fragment_scanner_layout.*


class ExampleScanItemsFragment : BaseScannerFragment() {

    companion object {
        val TAG = this::class.java.simpleName
    }

    private val viewModel = ExampleScanItemsViewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activity?.run {
            viewModel.activity = this
        } ?: run { throw Exception("Invalid Activity") }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        titleTextString = "Scan Kode Barang"
        subtitleTextString = "Nurunin Barang dari Truk"
        bindViewModel()
        viewModel.initItems()
    }

    override fun onResume() {
        super.onResume()
        thisContext(this::class.java)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        viewModel.saveModelData()
    }

    override fun didSetCode(code: String) {
        shout("did set code", code)
        viewModel.appendNewItem(code)
    }

    override fun setupFragmentListResult(): Fragment {
        return ListPakcageFragment()
    }

    override fun setupFragmentListResultTag(): String {
        return ListPakcageFragment.TAG
    }

    override fun setupScanFragmentTag(): String {
        return TAG
    }

    private fun bindViewModel() {
        viewModel.listOfDidScanItems.bind {
            tx_items_counter.text = viewModel.formattedItemCounter
            if (it.isEmpty()) {
                tx_last_scan_container.gone()
                return@bind
            }
            tx_last_scan_container.visible()
            tx_last_scan.text = viewModel.lastScanCodeLabel
        }
    }

}



