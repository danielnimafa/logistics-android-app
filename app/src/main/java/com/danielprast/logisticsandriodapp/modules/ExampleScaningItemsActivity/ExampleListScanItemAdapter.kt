package com.danielprast.logisticsandriodapp.modules.ExampleScaningItemsActivity

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.danielprast.logisticsandriodapp.R
import com.danielprast.logisticsandriodapp.models.ExampleDataModel
import com.danielprast.logisticsandriodapp.utils.extension.ctx
import com.danielprast.logisticsandriodapp.utils.shout
import com.google.gson.Gson


class ExampleListScanItemAdapter() : RecyclerView.Adapter<ExampleItemViewHolder>() {

    var removeEvent: ((Int, ExampleDataModel.Item?) -> Unit)? = null
    private var dataSource: MutableList<ExampleDataModel.Item> = mutableListOf()

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ExampleItemViewHolder {
        val view = LayoutInflater.from(parent.ctx)
            .inflate(
                R.layout.row_route_container_append_layout,
                parent,
                false
            )

        return ExampleItemViewHolder(view).apply {
            setupViewModel()
            setupUIAction(removeEvent)
        }
    }


    override fun getItemCount(): Int {
        return dataSource.size
    }


    override fun onBindViewHolder(
        holder: ExampleItemViewHolder,
        position: Int
    ) {
        holder.bindData(getPaket(position))
    }


    fun getPaket(position: Int): ExampleDataModel.Item {
        return dataSource[position]
    }


    fun updateDataSource(list: List<ExampleDataModel.Item>) {
        this.dataSource.clear()
        this.dataSource.addAll(list)
        notifyDataSetChanged()
        shout("adapter", "datasource updated ${this.dataSource.size}")
        shout("adapter", Gson().toJson(this.dataSource))
    }

    fun getDatasource(): List<ExampleDataModel.Item> {
        return dataSource
    }

    fun removeItem(position: Int): Boolean {
        if (dataSource.size > 0) {
            dataSource.removeAt(position)
            return true
        }
        return false
    }

    fun removeItemView(position: Int) {
        notifyItemRemoved(position)
        notifyItemRangeChanged(position, itemCount)
    }

}