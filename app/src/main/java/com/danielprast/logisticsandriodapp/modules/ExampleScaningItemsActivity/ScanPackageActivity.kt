package com.danielprast.logisticsandriodapp.modules.ExampleScaningItemsActivity

import android.os.Build
import android.os.Bundle
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity
import com.danielprast.logisticsandriodapp.R
import com.danielprast.logisticsandriodapp.utils.Const
import com.danielprast.logisticsandriodapp.utils.PrefHelper


class ScanPackageActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_scan_package)

        PrefHelper.developmentMode = Const.DevMode.ENABLE

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            window.setFlags(
                WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS,
                WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS
            )
        }

        val fragment = ExampleScanItemsFragment()
        if (supportFragmentManager.findFragmentByTag(ExampleScanItemsFragment.TAG) == null) {
            supportFragmentManager.beginTransaction()
                .add(
                    R.id.fragment_container_layout,
                    fragment,
                    ExampleScanItemsFragment.TAG
                ).commit()
        }
    }

}



