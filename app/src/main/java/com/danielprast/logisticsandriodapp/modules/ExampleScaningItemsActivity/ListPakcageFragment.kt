package com.danielprast.logisticsandriodapp.modules.ExampleScaningItemsActivity

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.danielprast.logisticsandriodapp.R
import com.danielprast.logisticsandriodapp.models.ExampleDataModel
import com.danielprast.logisticsandriodapp.utils.extension.*
import com.danielprast.logisticsandriodapp.utils.thisContext
import com.danielprast.logisticsandriodapp.viewModels.ExampleScanItemsViewModel
import com.danielprast.logisticsandriodapp.views.GridSpacingItemDecoration
import kotlinx.android.synthetic.main.fragment_list_package.*
import kotlinx.android.synthetic.main.message_info_layout.*


class ListPakcageFragment : Fragment() {

    companion object {
        val TAG = this::class.java.simpleName
    }

    private var viewModel = ExampleScanItemsViewModel()
    private val rviewAdapter =
        ExampleListScanItemAdapter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activity?.run {
            viewModel.activity = this
        } ?: run { throw Exception("Invalid Activity") }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(
            R.layout.fragment_list_package,
            container,
            false
        )
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        activity?.run {
            topSpace.layoutParams.height = getStatusBarHeight()
            bottomSpace.layoutParams.height = if (hasNavBar(resources)) getNavBarHeight() else 0
            reloadBtn.gone()
            messageInfoTx.text = stringGet(R.string.message_empty_scanned_container)

            rviewAdapter.removeEvent = { position, item ->
                didTapItemRview(position, item)
            }

            rview.apply {
                adapter = rviewAdapter
                setHasFixedSize(true)
                layoutManager = LinearLayoutManager(
                    this@run,
                    RecyclerView.VERTICAL,
                    false
                )
                addItemDecoration(
                    GridSpacingItemDecoration(
                        1,
                        dpToPx(18),
                        true
                    )
                )
            }

            backBtn.click {
                onBackPressed()
            }

            bindViewModel()
            viewModel.initItems()
            rviewAdapter.updateDataSource(viewModel.listOfDidScanItems.value)
        }
    }

    private fun didTapItemRview(
        position: Int,
        item: ExampleDataModel.Item?
    ) {
        if (rviewAdapter.removeItem(position)) {
            rviewAdapter.removeItemView(position)
            viewModel.updateListOfDidScanItems(rviewAdapter.getDatasource())
            viewModel.saveModelData()
        }
    }

    override fun onResume() {
        super.onResume()
        thisContext(this::class.java)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        viewModel.saveModelData()
    }

    override fun onDestroy() {
        super.onDestroy()
        viewModel.onCleared()
    }

    private fun bindViewModel() {
        viewModel.listOfDidScanItems.bind {
            showMessage(it.isEmpty())
        }
    }

    private fun showMessage(show: Boolean) {
        if (show) {
            messageInfoTx.visible()
            rview.gone()
            return
        }
        messageInfoTx.gone()
        rview.visible()
    }

}