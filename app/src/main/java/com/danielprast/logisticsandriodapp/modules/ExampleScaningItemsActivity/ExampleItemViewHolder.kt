package com.danielprast.logisticsandriodapp.modules.ExampleScaningItemsActivity

import android.view.View
import androidx.appcompat.widget.AppCompatImageButton
import androidx.appcompat.widget.AppCompatTextView
import androidx.recyclerview.widget.RecyclerView
import com.danielprast.logisticsandriodapp.R
import com.danielprast.logisticsandriodapp.models.ExampleDataModel
import com.danielprast.logisticsandriodapp.utils.extension.click
import com.danielprast.logisticsandriodapp.utils.extension.gone
import com.danielprast.logisticsandriodapp.utils.extension.visible
import com.danielprast.logisticsandriodapp.utils.shout
import com.danielprast.logisticsandriodapp.viewModels.ExampleItemScanViewModel


class ExampleItemViewHolder(val view: View) : RecyclerView.ViewHolder(view) {

    private var viewModel: ExampleItemScanViewModel? = ExampleItemScanViewModel()
    val containerTx: AppCompatTextView = view.findViewById(R.id.container_name_tx)
    val containerWarningMessageTx: AppCompatTextView =
        view.findViewById(R.id.container_warning_message_tx)
    val removeBtn: AppCompatImageButton = view.findViewById(R.id.remove_btn)


    fun setupViewModel() {
        viewModel?.code?.bind {
            containerTx.text = it
        }

        viewModel?.warningMessage?.bind {
            shout("binding view holder waning message", it)
            containerWarningMessageTx.text = it
            if (it.isNotEmpty()) {
                showWarningMessage(true)
                return@bind
            }

            showWarningMessage(false)
        }
    }


    fun setupUIAction(clickEvent: ((Int, ExampleDataModel.Item?) -> Unit)?) {
        removeBtn.click {
            clickEvent?.invoke(adapterPosition, viewModel?.getDataContainer())
        }
    }


    fun bindData(container: ExampleDataModel.Item) {
        viewModel?.updateContainer(container)
    }


    private fun showWarningMessage(state: Boolean) {
        if (state) {
            containerWarningMessageTx.visible()
            return
        }

        containerWarningMessageTx.gone()
    }

}