package com.danielprast.logisticsandriodapp.views

import android.content.Context
import com.kaopiz.kprogresshud.KProgressHUD

class PopupProgress(private val context: Context) {

    private var hud = KProgressHUD.create(context)
            .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
            .setAnimationSpeed(1)
            .setCancellable(false)

    fun showProgress(label: String? = null) {
        hideProgress()
        label?.also { hud?.setLabel(label) }
        hud?.show()
    }

    fun hideProgress() = hud?.dismiss()
}