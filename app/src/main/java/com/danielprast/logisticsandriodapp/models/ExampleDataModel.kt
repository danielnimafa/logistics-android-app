package com.danielprast.logisticsandriodapp.models

class ExampleDataModel {
    data class ScanItemsData(
        var items: MutableList<Item> = mutableListOf()
    )

    data class Item(
        var code: String = ""
    )
}